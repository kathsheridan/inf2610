#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int tailleTampon = 0;
char *tampon;

int ic = 0;
int ip = 0;

int lettresProduits = 0;
int lettresConsommes = 0;

sem_t libre;
sem_t occupe;
sem_t mutex;
bool flag_de_fin = false;


// fonction exécutée par les producteurs
void *producteur(void *pid) {
   int producteurTotale = 0;
   while (1) {;
      sem_wait(&libre);
      sem_wait(&mutex);

      srand(time(NULL));
      char lettre = ('A'+rand())%26;
      lettresProduits = lettresProduits + 1;
      producteurTotale = producteurTotale +1;
      tampon[ip] = lettre;

      ip = (ip + 1) % tailleTampon;

      sem_post(&mutex);
      sem_post(&occupe);
      if (flag_de_fin) {
         printf("Producteur %d a produit %d lettres.\n", pid, producteurTotale);
         pthread_exit(NULL);
      }
   }
   return NULL;
}

// fonction exécutée par les consommateurs
void *consommateur(void *cid) {
   int consommateurTotale =0;
   while (1) {
      int lettreRetire;
      sem_wait(&occupe);
      sem_wait(&mutex);

      lettreRetire = tampon[ic];
      lettresConsommes = lettresConsommes + 1;
      consommateurTotale = consommateurTotale +1;

      ic = (ic + 1) % tailleTampon;

      sem_post(&mutex);
      sem_post(&libre);
      if (lettreRetire == 0) {;
         printf("Consommateur %d a consomme %d lettres.\n", cid, consommateurTotale);
         pthread_exit(NULL);
      }
   }
   return NULL;
}

void sig_handler(int signum) {
   flag_de_fin = true;
}

int main(int argc, char *argv[]) {
   int nbProd = atoi(argv[1]);
   int nbCons = atoi(argv[2]);
   tailleTampon = atoi(argv[3]);

   tampon = calloc(tailleTampon, sizeof(int));

   sem_init(&libre, 0, tailleTampon);
   sem_init(&occupe, 0, 0);
   sem_init(&mutex, 0, 1);

   pthread_t producteurs[nbProd];
   pthread_t consommateurs[nbCons];
   for (int i = 0; i < nbProd; i++) {
      pthread_create(&producteurs[i], NULL, producteur, (void *)i);
   }
   for (int i = 0; i < nbCons; i++) {
      pthread_create(&consommateurs[i], NULL, consommateur, (void *)i);
   }

   signal(SIGALRM, sig_handler);
   alarm(1);

   // attente fin des producteurs
   for (int i = 0; i < nbProd; i++)
   {
      pthread_join(producteurs[i], NULL);
   }

   for (int i = 0; i < nbCons; i++)
   {
      sem_wait(&libre);
      sem_wait(&mutex);
      tampon[ip] = 0;
      ip = (ip + 1) % tailleTampon;
      sem_post(&mutex);
      sem_post(&occupe);
   }

   // attente fin des consommateurs
   for (int i = 0; i < nbCons; i++)
   {
      pthread_join(consommateurs[i], NULL);
   }

   printf("Lettres produites : %d", lettresProduits);
   printf("\n");
   printf("Lettres consomme : %d", lettresConsommes - nbCons);
   printf("\n");
   if (lettresProduits == lettresConsommes - nbCons) {
      printf("nice! Les lettres produits et consommes sont identiques !\n");
   } else {
      printf("erreur - les lettres produits et consommes sont differents.\n");
   }

   return 0;
}