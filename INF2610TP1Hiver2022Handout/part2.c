/*
 * processlab - part2.c
 * 
 * Ecole polytechnique de Montreal, Hiver 2022
 * Sabri, Khaoula -
 * Sheridan, Katherine - 1943785
 * GR. 03
*/

#include "libprocesslab/libprocesslab.h"


// TODO
// Si besoin, ajouter ici les directives d'inclusion
// -------------------------------------------------
#include <stdio.h>
#include <unistd.h>
#include<sys/wait.h>
#include <stdlib.h>
// -------------------------------------------------

void question2( )
{
    // TODO
    int status1;
    int status2;
    //level 0
    registerProc(0, 0, getpid(), getppid());

    if (fork() ==0 ) 
    {
    //level 1.1
        registerProc(1, 1, getpid(), getppid());
        if (fork() ==0) // level 2.1
        {
        registerProc(2,1,getpid(), getppid());
        printf("level2.1\n");
        exit(1);
        }
        wait(NULL);
        if (fork() ==0) // level 2.2
        {
        registerProc(2,2,getpid(), getppid());
        printf("level2.2\n");
        exit(1);
        }
        wait(NULL);
        if (fork() ==0) // level 2.3
        {
        registerProc(2,3,getpid(), getppid());
        printf("level2.3\n");
        exit(1);
        }
        wait(NULL);
        if (fork() ==0) // level 2.4
        {
        registerProc(2,4,getpid(), getppid());
        printf("level2.4\n");
        exit(1);
        }
        wait(NULL);
                    
    } else {
        printf("level1.1\n");
        pid_t childPid = wait(&status1);
        int childReturnValue1;
        while (childPid > 0) {
        childReturnValue1 += WEXITSTATUS(status1);
        }
        exit (childReturnValue1 +1);
       
    }
        
    //level 1.2
    if (fork() ==0 ) 
    {
        registerProc(1, 2, getpid(), getppid());
        if (fork() ==0) // level 2.5
        {
        registerProc(2,5,getpid(), getppid());
        printf("level2.5\n");
        exit(1);
        }
        wait(NULL);
        if (fork() ==0) // level 2.6
        {
        registerProc(2,6,getpid(), getppid());
        printf("level2.6\n");
        exit(1);
        }
        wait(NULL);
        if (fork() ==0) // level 2.7
        {
        registerProc(2,7,getpid(), getppid());
        printf("level2.7\n");
        exit(1);
        }
        wait(NULL);
    } else {
        printf("level1.2\n");
        pid_t childPid = wait(&status2);
        int childReturnValue2 ;
        while (childPid > 0) {
        childReturnValue2 += WEXITSTATUS(status2);
        }                     
        exit (childReturnValue2 +1);
                    
    }
	printProcRegistrations();
    char* programName = "ls";
    char* argument = "-l";
    execlp(programName, programName, argument, NULL);
    exit(1);
}
