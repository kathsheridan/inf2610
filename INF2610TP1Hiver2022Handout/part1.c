/*
 *  part1.c
 * 
 * Ecole polytechnique de Montreal, Hiver 2022
 * Sabri, Khaoula -
 * Sheridan, Katherine - 1943785
 * GR. 03
 */

// TODO
// Si besoin, ajouter ici les directives d'inclusion
// -------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
// -------------------------------------------------

#include "./libprocesslab/libprocesslab.h"

void question1(int choix) {
    // TODO
    if (choix == 1) {
        printf("75dbcb01f571f1c32e196c3a7d27f62e (printed using printf)");
        write(STDOUT_FILENO, "75dbcb01f571f1c32e196c3a7d27f62e (printed using write)\n", strlen("75dbcb01f571f1c32e196c3a7d27f62e (printed using write)\n"));
        printf("\n");
    }
    if (choix == 2) {
        setvbuf(stdout, NULL, _IONBF, 0);
        printf("75dbcb01f571f1c32e196c3a7d27f62e (printed using printf)");
        printf("\n");
        write(STDOUT_FILENO, "75dbcb01f571f1c32e196c3a7d27f62e (printed using write)\n", strlen("75dbcb01f571f1c32e196c3a7d27f62e (printed using write)\n"));
    }
    else { return; }

    
}   
    
