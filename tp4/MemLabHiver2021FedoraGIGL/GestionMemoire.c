#include "./libs/lib.h"
#define TAILLE_PAGE 1024

unsigned int calculerNumeroDePage(unsigned long adresse) {
	return adresse/TAILLE_PAGE;
}

unsigned long calculerDeplacementDansLaPage(unsigned long adresse) {
	return adresse%TAILLE_PAGE;
}

unsigned long calculerAdresseComplete(unsigned int numeroDePage, unsigned long deplacementDansLaPage) {
	return numeroDePage*TAILLE_PAGE+deplacementDansLaPage;
}

void rechercherTLB(struct RequeteMemoire* req, struct SystemeMemoire* mem) {
	unsigned int dansTLB=0;
	unsigned int numeroDePage=calculerNumeroDePage(req->adresseVirtuelle);
	unsigned int deplacement=calculerDeplacementDansLaPage(req->adresseVirtuelle);

	for(unsigned int i=0;i<TAILLE_TLB;i++) {
		if((mem->tlb->numeroPage[i] == numeroDePage)&& mem->tlb->entreeValide[i]) {
			dansTLB=1;
			int cadre = mem->tlb->numeroCadre[i];
			req->adressePhysique=calculerAdresseComplete(cadre, deplacement);
			req->estDansTablePages=0;
			req->estDansTLB=1;
			mem->tlb->dernierAcces[i]=req->date;
			break;
		}
	}

	if(!dansTLB) {
		req->adressePhysique=0;
		req->estDansTLB=0;
	}
}

void rechercherTableDesPages(struct RequeteMemoire* req, struct SystemeMemoire* mem) {
	unsigned int numeroPage=calculerNumeroDePage(req->adresseVirtuelle);
	unsigned int deplacement=calculerDeplacementDansLaPage(req->adresseVirtuelle);

	if(mem->tp->entreeValide[numeroPage]) {
		int cadre=mem->tp->numeroCadre[numeroPage];
		req->adressePhysique=calculerAdresseComplete(cadre, deplacement);
	} else {
		req->adressePhysique=0;
	}
}

void ajouterDansMemoire(struct RequeteMemoire* req, struct SystemeMemoire* mem) {
	for (int i=0;i<TAILLE_MEMOIRE;i++){
		if(!mem->memoire->utilisee[i]) {
			unsigned int numeroDePage=calculerNumeroDePage(req->adresseVirtuelle);
			unsigned int deplacement=calculerDeplacementDansLaPage(req->adresseVirtuelle);
			req->adressePhysique=calculerAdresseComplete(i, deplacement);
			mem->memoire->utilisee[i]=1;
			mem->memoire->dernierAcces[i]=req->date;
			mem->memoire->dateCreation[i]=req->date;
			mem->memoire->numeroPage[i]=numeroDePage;
			break;
		}
	}
}

void mettreAJourTLB(struct RequeteMemoire* req, struct SystemeMemoire* mem) {
	unsigned int index=0;
	unsigned int plein=1;
	for(int i=0;i<TAILLE_TLB;i++) {
		if(mem->tlb->entreeValide[i]==0) {
			index=i;
			plein=0;
			break;
		}
	}

	if(plein) {
		unsigned int vieux = mem->tlb->dateCreation[0];
		for(int i=0;i<TAILLE_TLB;i++) {
			if (vieux>mem->tlb->dateCreation[i]) {
				index=i;
				vieux = mem->tlb->dateCreation[i];
			}
		}
	}
	mem->tlb->numeroPage[index]=calculerNumeroDePage(req->adresseVirtuelle);
	mem->tlb->numeroCadre[index]=calculerNumeroDePage(req->adressePhysique);
	mem->tlb->dernierAcces[index]=req->date;
	mem->tlb->dateCreation[index]=req->date;
	mem->tlb->entreeValide[index]=1;
}

// NE PAS MODIFIER
int main() {
    evaluate(
		&calculerNumeroDePage, 
		&calculerDeplacementDansLaPage, 
		&calculerAdresseComplete, 
        &rechercherTLB, 
		&rechercherTableDesPages,
		&mettreAJourTLB,
		&ajouterDansMemoire
    );
    return 0;
}
